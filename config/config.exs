import Config
config :ecto_demo, EctoDemo.Repo,
  show_sensitive_data_on_connection_error: true,
  database: "root",
  socket_dir: "/run/postgresql",
  username: "root"
  # password: "pass",
  # hostname: "localhost"
config :ecto_demo,
  ecto_repos: [EctoDemo.Repo]
