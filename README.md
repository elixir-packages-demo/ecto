# ecto

![Hex.pm](https://img.shields.io/hexpm/dw/ecto)
Toolkit for data mapping and language integrated query. [ecto](https://hex.pm/packages/ecto)

* [Getting Started](https://hexdocs.pm/ecto/getting-started.html)
